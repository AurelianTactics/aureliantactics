﻿using UnityEngine;
using System.Collections.Generic;
using System;

//object for saving the user's current story progress
[Serializable]
public class StorySave {

    public int StoryId { get; set; } //story that this save is associated with
    public int StoryInt { get; set; } //progress in that story
    public int Gold { get; set; } //gold party has
    public List<StoryPointObject> storyPointObjectList; //need it here to know what cutScenes and battles have been consumed
    public int StorySaveId { get; set; } //assigned when the story is actually saved to prevent over-writing
    public List<PlayerUnit> unitList;

    //unit list
    //item list
    //experience/job points

    //item list: how to story when saved, how to juggle equipped/not equipped items
//experience: have free XP(can be used on anyone) and unit specfic XP
//job points? how to spend points on unit to purchase abilities(for now maybe just combine//with XP)

    public StorySave( int zStoryId, int zStoryInt, int zGold, List<StoryPointObject> zList, List<PlayerUnit> puList )
    {
        this.StoryId = zStoryId;
        this.StoryInt = zStoryInt;
        this.Gold = zGold;
        this.storyPointObjectList = zList;
        this.StorySaveId = NameAll.NULL_INT;
        this.unitList = puList;
    }

    //turns battle and/or cutscene off
    public void ConsumeStoryPointInt(StoryPointInt spi, bool isConsumeBattle, bool isConsumeCutScene)
    {
        foreach (StoryPointObject spo in this.storyPointObjectList)
        {
            if (spi.StoryPointId == spo.PointId)
            {
                if(spo.storyIntList.Contains(spi))
                {
                    if (isConsumeBattle)
                        spi.IsHasStoryBattle = false;
                    if (isConsumeCutScene)
                        spi.IsHasCutScene = false;
                    return;
                }
                return;
            }
        }
    }
    
    public Dictionary<int,PlayerUnit> GetPlayerUnitDict()
    {
        Dictionary<int,PlayerUnit> unitDict = new Dictionary<int, PlayerUnit>();
        for (int i = 0; i < unitList.Count; i++)
        {
            unitDict.Add(i, unitList[i]);
        }

        return unitDict;
    }

    public void UnitDictToList(Dictionary<int,PlayerUnit> unitDict)
    {
        this.unitList.Clear();
        foreach(KeyValuePair<int,PlayerUnit> kvp in unitDict)
        {
            this.unitList.Add(kvp.Value);
        }
    }
}
