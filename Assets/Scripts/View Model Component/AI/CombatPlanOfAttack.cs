﻿using UnityEngine;
using System.Collections;

public class CombatPlanOfAttack
{
    public SpellName spellName;
    public Targets target;
    public Point moveLocation;
    public Point fireLocation;
    public Directions attackDirection;
    public bool isActFirst;

    public CombatPlanOfAttack()
    {
        this.spellName = null;
        //this.target = null;
        //this.moveLocation = null;
        //this.fireLocation = null;
        //this.attackDirection = null;
        this.isActFirst = false;
    }

    public void POASummary()
    {
        if( this.spellName == null)
        {
            Debug.Log("poa spellName is null");
        }
        else if( this.moveLocation == null)
        {
            Debug.Log("poa moveLocation is null");
        }
        else if (this.fireLocation == null)
        {
            Debug.Log("poa fireLocation is null");
        }
        else
        {
            Debug.Log("" + spellName.AbilityName + ", " + target.ToString() + ", "
                        + moveLocation.ToString() + ", " + fireLocation.ToString() + ", " + attackDirection.ToString() + ", " + isActFirst);
        }
        
    }
}

