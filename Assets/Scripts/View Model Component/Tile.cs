﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour 
{
    [SerializeField]
    Material material;
    [SerializeField]
    Texture team2Highlight;
    [SerializeField]
    Texture team3Highlight;
    [SerializeField]
    Texture tileHighlight;

    #region Const
    public const float stepHeight = 0.25f;
    public const float centerHeight = 0.05f;//used for centering the tile sho the indicator shows above the default indicators
	#endregion

	#region Fields / Properties
	public Point pos;
	public int height;
	public Vector3 center { get { return new Vector3(pos.x, height * stepHeight + centerHeight, pos.y); }}
	public GameObject content;
	[HideInInspector] public Tile prev;
	[HideInInspector] public int distance;

    private int unitId = NameAll.NULL_UNIT_ID; //added to easier check tiles for PlayerUnits
    public int UnitId
    {
        get { return unitId; }
        set { unitId = value; }
    }

    private int pickUpId = 0; //0 for nothing, 1 for crystals
    public int PickUpId
    {
        get { return pickUpId;}
        set { pickUpId = value; }
    }
    #endregion

    #region Public
    public void Grow ()
	{
		height++;
		Match();
	}
	
	public void Shrink ()
	{
		height--;
		Match ();
	}

	public void Load (Point p, int h)
	{
		pos = p;
		height = h;
		Match();
	}
	
	public void Load (Vector3 v)
	{
		Load (new Point((int)v.x, (int)v.z), (int)v.y);
        this.UnitId = NameAll.NULL_UNIT_ID;
	}

    //change tile look for MapBuilder
    public void RevertTile()
    {
        var rend = this.gameObject.GetComponent<Renderer>();
        rend.material = material;
    }

    public void HighlightTile(int teamId)
    {
        if (teamId == 2)
        {
            var rend = this.gameObject.GetComponent<Renderer>();
            //rend.material.mainTexture = Resources.Load("square frame 1") as Texture;
            rend.material.mainTexture = team2Highlight;
        }
        else if (teamId == 3)
        {
            var rend = this.gameObject.GetComponent<Renderer>();
            //rend.material.mainTexture = Resources.Load("square frame 2") as Texture;
            rend.material.mainTexture = team3Highlight;
        }
        else
        {
            var rend = this.gameObject.GetComponent<Renderer>();
            //rend.material.mainTexture = Resources.Load("square frame 3") as Texture;
            rend.material.mainTexture = tileHighlight;
        }
    }
    #endregion

    #region Private
    void Match ()
	{
		transform.localPosition = new Vector3( pos.x, height * stepHeight / 2f, pos.y );
		transform.localScale = new Vector3(1, height * stepHeight, 1);
	}

    //private IEnumerator AutoRevert()
    //{
    //    yield return new WaitForSeconds(3);
    //    RevertTile();
    //}
    #endregion

    public string GetTileSummary()
    {
        return " (" + this.pos.x + "," + this.pos.y + ")" + " unitID: " + this.unitId + " height: " + this.height;
    }
}
