﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Board : MonoBehaviour 
{
	#region Fields / Properties
	[SerializeField] GameObject tilePrefab;
	public Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();
	public Point min { get { return _min; }}
	public Point max { get { return _max; }}
	Point _min;
	Point _max;
	Point[] dirs = new Point[4]
	{
		new Point(0, 1),
		new Point(0, -1),
		new Point(1, 0),
		new Point(-1, 0)
	};
	Color selectedTileColor = new Color(0, 1, 1, 1);
	Color defaultTileColor = new Color(1, 1, 1, 1);
    Color highlightTileColor = new Color(0, 1, 1, 1);

    [SerializeField]
    GameObject groundCrystalPrefab;
    private Dictionary<Tile, GameObject> groundObjectDict = new Dictionary<Tile, GameObject>(); //key is maptileindex, value is the type of object it is
    #endregion

    #region Public
    public void Load (LevelData data)
	{
		_min = new Point(int.MaxValue, int.MaxValue);
		_max = new Point(int.MinValue, int.MinValue);
		
		for (int i = 0; i < data.tiles.Count; ++i)
		{
			GameObject instance = Instantiate(tilePrefab) as GameObject;
			instance.transform.SetParent(transform);
			Tile t = instance.GetComponent<Tile>();
			t.Load(data.tiles[i]);
			tiles.Add(t.pos, t);
			
			_min.x = Mathf.Min(_min.x, t.pos.x);
			_min.y = Mathf.Min(_min.y, t.pos.y);
			_max.x = Mathf.Max(_max.x, t.pos.x);
			_max.y = Mathf.Max(_max.y, t.pos.y);
		}
	}

    public Tile GetTile(int x, int y)
    {
        Point p = new Point(x, y);
        return tiles.ContainsKey(p) ? tiles[p] : null;
    }

    public Tile GetTile (Point p)
	{
		return tiles.ContainsKey(p) ? tiles[p] : null;
	}

    public Tile GetTile(Tile t)
    {
        return tiles.ContainsKey(t.pos) ? tiles[t.pos] : null;
    }

    public Tile GetTile(PlayerUnit target)
    {
        Point p = new Point();
        p.x = target.TileX;
        p.y = target.TileY;
        return GetTile(p);
    }

    public List<Tile> Search(Tile start, Func<Tile, Tile, bool> addTile)
    {
        List<Tile> retValue = new List<Tile>();
        retValue.Add(start);

        ClearSearch();
        Queue<Tile> checkNext = new Queue<Tile>();
        Queue<Tile> checkNow = new Queue<Tile>();

        start.distance = 0;
        checkNow.Enqueue(start);

        while (checkNow.Count > 0)
        {
            Tile t = checkNow.Dequeue();
            for (int i = 0; i < 4; ++i)
            {
                Tile next = GetTile(t.pos + dirs[i]);
                if (next == null || next.distance <= t.distance + 1)
                    continue;

                if (addTile(t, next))
                {
                    next.distance = t.distance + 1;
                    next.prev = t;
                    checkNext.Enqueue(next);
                    retValue.Add(next);
                }
            }

            if (checkNow.Count == 0)
                SwapReference(ref checkNow, ref checkNext);
        }

        return retValue;
    }

    public void SelectTiles (List<Tile> tiles)
	{
		for (int i = tiles.Count - 1; i >= 0; --i)
        {
            //tiles[i].GetComponent<Renderer>().material.SetColor("_Color", selectedTileColor);
            tiles[i].HighlightTile(0);
        }
			
	}

	public void DeSelectTiles (List<Tile> tiles)
	{
		for (int i = tiles.Count - 1; i >= 0; --i)
        {
            //tiles[i].GetComponent<Renderer>().material.SetColor("_Color", defaultTileColor);
            tiles[i].RevertTile();
        }
			
	}

    public void UpdatePlayerUnitTile(PlayerUnit pu, Tile newTile)
    {
        Point p = new Point(pu.TileX, pu.TileY);
        GetTile(p).UnitId = NameAll.NULL_UNIT_ID;
        GetTile(newTile.pos).UnitId = pu.TurnOrder;
    }

    public void UpdatePlayerUnitTileSwap(PlayerUnit pu, Tile newTile)
    {
        //Point p = new Point(pu.TileX, pu.TileY); //this tile has already been updated with the actor who swapped in
        //GetTile(p).UnitId = NameAll.NULL_UNIT_ID;
        GetTile(newTile.pos).UnitId = pu.TurnOrder;
    }

    //called in combatState to highlight certain tiles
    public void HighlightTile(Point p, int teamId, bool doHighlight)
    {
        Tile t = GetTile(p);
        if (t != null)
        {
            if (doHighlight)
            {
                //t.GetComponent<Renderer>().material.SetColor("_Color", highlightTileColor);
                //Debug.Log("highlighting tile");
                t.HighlightTile(teamId);
            }
            else
            {
                //t.GetComponent<Renderer>().material.SetColor("_Color", defaultTileColor);
                t.RevertTile();
            }
        }
    }

    //get reflect tile
    public Tile GetReflectTile(PlayerUnit actor, PlayerUnit target)
    {
        int cast_x = actor.TileX;
        int cast_y = actor.TileY;
        int target_x = target.TileX;
        int target_y = target.TileY;

        int x1, y1;
        if (cast_x <= target_x)
        {
            x1 = target_x + Math.Abs(target_x - cast_x);
        }
        else {
            x1 = target_x - Math.Abs(target_x - cast_x);
        }
        if (cast_y <= target_y)
        {
            y1 = target_y + Math.Abs(target_y - cast_y);
        }
        else {
            y1 = target_y - Math.Abs(target_y - cast_y);
        }
        Point p = new Point(x1, y1);
        return GetTile(p);
    }

    //find the tile furtherst away from the nearest enemey
    //in future can be used to find tile furthest away from all enemies
    public Point GetFarthestPoint( List<Tile> moveOptions, Tile nearestFoe, Tile currentTile )
    {
        Point retValue = currentTile.pos;
        int z1 = moveOptions.Count;
        int zMax = 0;
        for( int i = 0; i < z1; i++)
        {
            int z2 = Math.Abs(nearestFoe.pos.x - moveOptions[i].pos.x) + Math.Abs(nearestFoe.pos.y - moveOptions[i].pos.y);
            if( z2 > zMax)
            {
                retValue = moveOptions[i].pos;
            }
        }

        return retValue;
    }

    //get mimetargettile, called in CalcResolveAction
    public Tile GetMimeTargetTile(PlayerUnit actor, PlayerUnit mime, Tile targetTile)
    {
        Tile actorTile = GetTile(actor);
        Tile mimeTile = GetTile(mime);

        //targeting self, retur the mime coordinates
        if ( actorTile == targetTile)
        {
            return mimeTile;
        }

        int forwardOffset;
        int sideOffset;
        int xOffset = Mathf.Abs(targetTile.pos.x - actorTile.pos.x);
        int yOffset = Mathf.Abs(targetTile.pos.y - actorTile.pos.y);
        bool isRight; //is sideOffset to the right or left of the actor. if dead on, this doesn't matter (just adding 0)
        int xMimeChange = 0; //added at the end to get the new point
        int yMimeChange = 0; //added at the end to get the new point

        //gets the direction the actor will be facing
        Directions attackDir = actorTile.GetDirectionAttack(targetTile);
        
        if ( attackDir == Directions.East )
        {
            //side offset is >= N/S offset, thus the forward offset is the xOffset
            forwardOffset = xOffset;
            sideOffset = yOffset;
            if (targetTile.pos.y < actorTile.pos.y)
                isRight = true;
            else
                isRight = false;
        }
        else if (attackDir == Directions.West)
        {
            //side offset is >= N/S offset, thus the forward offset is the xOffset
            forwardOffset = xOffset;
            sideOffset = yOffset;
            if (targetTile.pos.y > actorTile.pos.y)
                isRight = true;
            else
                isRight = false;
        }
        else if (attackDir == Directions.North)
        {
            //side offset is < N/S offset, thus the forward offset is the y offset
            forwardOffset = yOffset;
            sideOffset = xOffset;
            if (targetTile.pos.x > actorTile.pos.x)
                isRight = true;
            else
                isRight = false;
        }
        else //SOUTH
        {
            //side offset is < N/S offset, thus the forward offset is the y offset
            forwardOffset = yOffset;
            sideOffset = xOffset;
            if (targetTile.pos.x < actorTile.pos.x)
                isRight = true;
            else
                isRight = false;
        }

        if (mime.Dir == Directions.East)
        {
            xMimeChange = forwardOffset;
            if (isRight)
                yMimeChange = sideOffset * -1; //off to the right is DOWN the y axis
            else
                yMimeChange = sideOffset;
            Debug.Log("E " + forwardOffset + " " + sideOffset);
        }
        else if (mime.Dir == Directions.West)
        {
            xMimeChange = forwardOffset * -1;
            if (isRight)
                yMimeChange = sideOffset; //off to the right is UP the y axis
            else
                yMimeChange = sideOffset * -1;
            Debug.Log("W " + forwardOffset + " " + sideOffset);
        }
        else if (mime.Dir == Directions.North)
        {
            yMimeChange = forwardOffset;
            if (isRight)
                xMimeChange = sideOffset; //off to the right is UP the x axis
            else
                xMimeChange = sideOffset * -1;
            Debug.Log("N " + forwardOffset + " " + sideOffset);
        }
        else //south
        {
            yMimeChange = forwardOffset * -1;
            if (isRight)
                xMimeChange = sideOffset * -1; //off to the right is DOWN the x axis
            else
                xMimeChange = sideOffset;
            Debug.Log("S " + forwardOffset + " " + sideOffset);
        }
    
        Point p = new Point(mimeTile.pos.x + xMimeChange, mimeTile.pos.y + yMimeChange);
        return GetTile(p); //can return null, null check done in CalcResolveAction
    }

    public void SetTilePickUp(int tileX, int tileY, bool isEnable, int pickUpId = 1)
    {
        Tile t = GetTile(tileX, tileY);
        if(isEnable)
        {
            t.PickUpId = pickUpId;   
        }
        else
        {
            t.PickUpId = 0;
        }

        if (pickUpId == 1)
        {
            SetPickUpObject(t, isEnable);
        }
    }

    public void DisableUnit(PlayerUnit pu)
    {
        GetTile(pu).UnitId = NameAll.NULL_UNIT_ID;
    }

    void SetPickUpObject(Tile t, bool isEnable)
    {
        if(isEnable)
        {
            GameObject go = Instantiate(groundCrystalPrefab) as GameObject;
            Vector3 vec = t.transform.position;
            vec += new Vector3(0, (vec.y + 0.3f), 0);
            go.transform.position = vec;
            groundObjectDict.Add(t,go);
        }
        else
        {
            Destroy(groundObjectDict[t] );
            groundObjectDict.Remove(t);
        }
    }

    //called in CombatMoveSequenceState for WindsOfFate, returns a random tile that doesn't have a unit on it
    public Point GetRandomPoint(Point p)
    {
        //Debug.Log("in get random point");
        var tempList = RandomValues(this.tiles).ToList();
        Tile t = tempList[0];

        if (t.UnitId == NameAll.NULL_UNIT_ID)
        {
            return t.pos;
        }

        return p; //defaults to entered value unless new one is found
    }

    //grabs a random entry for a dictionary
    public IEnumerable<TValue> RandomValues<TKey, TValue>(IDictionary<TKey, TValue> dict)
    {
        System.Random rand = new System.Random();
        List<TValue> values = Enumerable.ToList(dict.Values);
        int size = dict.Count;
        yield return values[rand.Next(size)];
        //while (true)
        //{
        //    yield return values[rand.Next(size)];
        //}
    }

    #endregion

    #region Private
    void ClearSearch ()
	{
		foreach (Tile t in tiles.Values)
		{
			t.prev = null;
			t.distance = int.MaxValue;
		}
	}

	void SwapReference (ref Queue<Tile> a, ref Queue<Tile> b)
	{
		Queue<Tile> temp = a;
		a = b;
		b = temp;
	}
	#endregion
}