﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CombatEndState : CombatState
{
    public override void Enter()
    {
        base.Enter();
        GameObject go = GameObject.Find("PlayerManagerObject(Clone)");
        Destroy(go);
        if ( !PlayerManager.Instance.IsOfflineGame() )
        {
            //go = GameObject.Find("ChatGameObject");
            //if (go != null)
            //{
            //    go.SetActive(false);
            //    //Debug.Log("why the fuck can't I leave the fucking chat channel?");
            //    //go.GetComponent<ChatHandler>().LeaveRoomChannels();
            //}
            //else
            //Debug.Log("ERROR: couldn't find chat game object before leaving scene");
            PhotonNetwork.LeaveRoom();
            SceneManager.LoadScene(NameAll.SCENE_MP_MENU);
        }
        else
        {
            if( PlayerPrefs.GetInt(NameAll.PP_COMBAT_ENTRY,NameAll.SCENE_MAIN_MENU) == NameAll.SCENE_STORY_MODE
                && owner.GetComponent<CombatVictoryCondition>().Victor == Teams.Team1 )
            {
                SceneManager.LoadScene(NameAll.SCENE_STORY_MODE);
            }
            else 
                SceneManager.LoadScene(NameAll.SCENE_MAIN_MENU);
        }
        
    }
}
