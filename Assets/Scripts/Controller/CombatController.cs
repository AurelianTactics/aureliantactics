﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatController : StateMachine
{
    //public CameraRig cameraRig;
    public UICameraMenu cameraMain;
    public Board board;
    //public LevelData levelData; //Might be able to comment this out
    public Transform tileSelectionIndicator;
    public Point pos;
    public Tile currentTile { get { return board.GetTile(pos); } }
    //public AbilityMenuPanelController abilityMenuPanelController; //can comment this out later, using activeMenu
    public UIAbilityScrollList abilityMenu;
    public UIActiveTurnMenu activeMenu;
    //public StatPanelController statPanelController; //can comment this out later, using actorPanel
    public CombatUITarget actorPanel;
    public CombatUITarget targetPanel;
    public CombatUITarget previewPanel;
    
    //public HitSuccessIndicator hitSuccessIndicator; //using preview panel
    public BattleMessageController battleMessageController;
    public FacingIndicator facingIndicator;
    //public Turn turn = new Turn();
    public CombatTurn turn = new CombatTurn();
    //public List<Unit> units = new List<Unit>();
    //public IEnumerator round;
    public CombatComputerPlayer cpu;
    public CalculationMono calcMono; //allows access to CalculationMono functions from any state

    public GameObject markerTeam1Prefab;
    public GameObject markerTeam2Prefab;

    void Start()
    {
        //cpu = new CombatComputerPlayer();
        ChangeState<InitCombatState>();
    }
}