﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EckTechGames.FloatingCombatText;
//using ExitGames = ExitGames.Client.Photon.Hashtable;

public class PlayerManager : Singleton<PlayerManager>
{

    private static List<PlayerUnit> sPlayerUnitList;
    private static List<GameObject> sPlayerObjectList;
    private static List<PlayerUnit> sGreenList;
    private static List<PlayerUnit> sRedList;

    private static bool isMimeOnGreen;// = false;
    private static bool isMimeOnRed;// = false;

    static int currentTick;
    const string TickMenuAdd = "TickMenu.AddItem"; //notification picked up to displayer current tick (UIMenuMenu)
    static readonly string TickKey = "roomTick";
    public static CombatMultiplayerObject sMPObject;

    PhotonView photonView;

    void Awake()
    {
        photonView = PhotonView.Get(this);
        currentTick = 0;
    }
    

    protected PlayerManager()
    { // guarantee this will be always a singleton only - can't use the constructor!
        //myGlobalVar = "asdf";
        sPlayerUnitList = new List<PlayerUnit>();
        sPlayerObjectList = new List<GameObject>();
        sGreenList = new List<PlayerUnit>();
        sRedList = new List<PlayerUnit>();
        sMPObject = new CombatMultiplayerObject();
    }

    //called in scene transitions, need to clear the lists
    public void ClearLists()
    {
        sPlayerUnitList = new List<PlayerUnit>();
        sPlayerObjectList = new List<GameObject>();
        sGreenList = new List<PlayerUnit>();
        sRedList = new List<PlayerUnit>();
    }

    #region PreCombat
    //PlayerManager is created in the pre-game (MPGameController) and persists through the scene load into combat

    [PunRPC]
    public void AddToListRPC(string puCode, int teamId, int type)
    {
        //Debug.Log("why is this beign called in Combat and from where?");
        PlayerUnit pu = CalcCode.BuildPlayerUnit(puCode);
        if( teamId == 2)
        {
            sGreenList.Add(pu);
        }
        else
        {
            sRedList.Add(pu);
        }
    }

    [PunRPC]
    public void RemoveFromListRPC(int teamId)
    {
        if (teamId == 2)
        {
            if (sGreenList.Count > 0)
            {
                sGreenList.RemoveAt(sGreenList.Count - 1);
            }
        }
        else
        {
            if (sRedList.Count > 0)
            {
                sRedList.RemoveAt(sRedList.Count - 1);
            }
        }
    }

    public void SetTeamList(List<PlayerUnit> puList, int teamId)
    {
        if( teamId == NameAll.TEAM_ID_GREEN)
        {
            sGreenList = puList;
        }
        else if (teamId == NameAll.TEAM_ID_RED)
        {
            sRedList = puList;
        }
    }

    public void EditTeamLists( PlayerUnit pu, int teamId, int type)
    {
        if( type == NameAll.TEAM_LIST_CLEAR)
        {
            sRedList.Clear();
            sGreenList.Clear();
            return;
        }

        if( type == NameAll.TEAM_LIST_ADD)
        {
            if(!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
            {
                string puCode = CalcCode.BuildStringFromPlayerUnit(pu);
                photonView.RPC("AddToListRPC", PhotonTargets.All, new object[] { puCode, teamId, NameAll.TEAM_LIST_ADD });
            }
            else
            {
                if (teamId == NameAll.TEAM_ID_GREEN)
                {
                    sGreenList.Add(pu);
                }
                else
                {
                    sRedList.Add(pu);
                }
            } 
        }
        else if( type == NameAll.TEAM_LIST_REMOVE)
        {
            if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
            {
                photonView.RPC("RemoveFromListRPC", PhotonTargets.All, new object[] { teamId });
            }
            else
            {
                if (teamId == NameAll.TEAM_ID_GREEN)
                {
                    if (sGreenList.Count > 0)
                    {
                        sGreenList.RemoveAt(sGreenList.Count - 1);
                    }
                }
                else
                {
                    if (sRedList.Count > 0)
                    {
                        sRedList.RemoveAt(sRedList.Count - 1);
                    }
                }
            }
        }
    }

    public List<PlayerUnit> GetTeamList(int teamId)
    {
        if (teamId == NameAll.TEAM_ID_GREEN)
        {
            return sGreenList;
        }
        else 
        {
            return sRedList;
        }
    }

    public void ClearTeamLists()
    {
        sGreenList.Clear();
        sRedList.Clear();
    }
    #endregion

    //units added in SetUpMap (to masterClient and P2)
    public void AddPlayerUnit(PlayerUnit pu)
    {
        sPlayerUnitList.Add(pu);
    }

    public void AddPlayerObject(GameObject po)
    {
        sPlayerObjectList.Add(po);
    }

    //doing it by index
    public PlayerUnit GetPlayerUnit( int unit_id)
    {
        //try
        //{
        //    return sPlayerUnitList[unit_id];
        //}
        //catch (ArgumentOutOfRangeException)
        //{
        //    Debug.Log("ERROR: GetPlayerUnit returns null " + unit_id);
        //    return null;
        //}
        //catch (IndexOutOfRangeException)
        //{
        //    Debug.Log("ERROR: GetPlayerUnit returns null " + unit_id);
        //    return null;
        //}
        return sPlayerUnitList[unit_id];
        //return null;
    }

    //doing it by index
    public GameObject GetPlayerUnitObject(int unit_id)
    {
        return sPlayerObjectList[unit_id];
        //try
        //{
        //    return sPlayerObjectList[unit_id];
        //}
        //catch (IndexOutOfRangeException)
        //{
        //    return null;
        //}

        //return null;
    }

    public PlayerUnitObject GetPlayerUnitObjectComponent(int unitId)
    {
        GameObject p = GetPlayerUnitObject(unitId);
        return p.GetComponent<PlayerUnitObject>();
    }

    public List<PlayerUnit> GetPlayerUnitList()
    {
        //List<PlayerUnit> puList = new List<PlayerUnit>();
        //foreach( PlayerUnit pu in sPlayerUnitList)
        //{
        //    if( !StatusManager.Instance.IfStatusByUnitAndId(pu.TurnOrder,NameAll.STATUS_ID_CRYSTAL))
        //    {
        //        puList.Add(pu);
        //    }
        //}
        //return puList;
        return sPlayerUnitList;
    }

    //increments the CT. 
    //non-essential part: updates current tick for testing and for MP display to test if host and client are in sync and on the same tick
    [PunRPC]
    public void IncrementCTPhase()
    {
        currentTick += 1;
        Dictionary<string, int> tempDict = new Dictionary<string, int>();
        tempDict.Add("currentTick", currentTick);

        foreach (PlayerUnit p in sPlayerUnitList)
        {
            p.AddCT(); //checks for statuses
        }

        if (!PhotonNetwork.offlineMode )
        {
            Room room = PhotonNetwork.room;
            if (PhotonNetwork.isMasterClient)
            {
                ExitGames.Client.Photon.Hashtable turnProps = new ExitGames.Client.Photon.Hashtable();
                turnProps[TickKey] = currentTick;
                room.SetCustomProperties(turnProps);

                photonView.RPC("IncrementCTPhase", PhotonTargets.Others, new object[] { });
            }
                
            if (room == null || room.customProperties == null || !room.customProperties.ContainsKey(TickKey))
            {
                tempDict.Add("roomTick", 0);
            }
            else
            {
                tempDict.Add("roomTick", (int)room.customProperties[TickKey]); 
            }

            
        }

        this.PostNotification(TickMenuAdd, tempDict);
    }

    public PlayerUnit GetNextActiveTurnPlayerUnit(bool isSetQuickFlagToFalse)
    {
        //loops through looking for quick flag and if CT over 100
        //units are ordered by turn order so just looking for the first one
        PlayerUnit retValue = null;
        int count = sPlayerUnitList.Count;
        for( int i = 0; i < count; i++)
        {
            //don't override the returnPU with a lower turn order unit, only override it with a quick flag unit
            PlayerUnit tempPU = GetPlayerUnit(i);
            if(tempPU.IsQuickFlag() && StatusManager.Instance.IsTurnActable(tempPU.TurnOrder))
            {
                if (isSetQuickFlagToFalse)
                    SetQuickFlag(tempPU.TurnOrder, false);
                return tempPU;
            }
            else if(retValue == null && tempPU.IsTurnActable()) 
            {
                retValue = tempPU;
            }
        }
        return retValue;
    }

    //gets next player turn, assumes players added to array in turn order
    //likely need add in status lab
    //public int GetNextTurnPlayerUnitId()
    //{
    //    foreach( PlayerUnit p in sPlayerUnitList)
    //    {
    //        if( p.IsTurnActable()) //vs. eligible. eligible ends the current player turn, actable never lets it happen
    //        {
    //            return p.TurnOrder;
    //        }
    //    }
    //    return NameAll.NULL_INT;
    //}

    //called in various calculations (like calculationresove
    //[PunRPC] //don't think I need this to be a RPC as both sides should be getting to this locally
    public void SetPlayerObjectAnimation(int unitId, string animation, bool isIdle)
    {
        //Debug.Log("receiving an animation for playerObject " + animation);
        PlayerUnitObject puo = GetPlayerUnitObject(unitId).GetComponent<PlayerUnitObject>();
        puo.SetAnimation(animation, isIdle);
        //if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        //{
        //    photonView.RPC("SetPlayerObjectAnimation", PhotonTargets.Others, new object[] { unitId, animation, isIdle });
        //}
    }

    //online: life and death sent through PunRPC
    [PunRPC]
    public void SendPlayerObjectAnimation(int unitId, string animation, bool isIdle)
    {
        //Debug.Log("receiving an animation for playerObject " + animation);
        if (!PhotonNetwork.offlineMode)
        {
            if (PhotonNetwork.isMasterClient)
            {
                photonView.RPC("SendPlayerObjectAnimation", PhotonTargets.Others, new object[] { unitId, animation, isIdle });
            }
            else
            {
                PlayerUnitObject puo = GetPlayerUnitObject(unitId).GetComponent<PlayerUnitObject>();
                puo.SetAnimation(animation, isIdle);
                if( animation == NameAll.ANIMATION_DEAD )
                    AddToStatusList(unitId, NameAll.STATUS_ID_DEAD_3);
                else if( animation == NameAll.ANIMATION_LIFE)
                {
                    RemoveFromStatusList(unitId, NameAll.STATUS_ID_DEAD_0);
                    RemoveFromStatusList(unitId, NameAll.STATUS_ID_DEAD_1);
                    RemoveFromStatusList(unitId, NameAll.STATUS_ID_DEAD_2);
                    RemoveFromStatusList(unitId, NameAll.STATUS_ID_DEAD_3);
                }
            }
        }
    }

    public void KnockbackPlayer(Board board, int unitId, Tile moveTile )
    {
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            int tileX = moveTile.pos.x;
            int tileY = moveTile.pos.y;
            photonView.RPC("KnockbackPlayerRPC", PhotonTargets.Others, new object[] { unitId, tileX, tileY });
        }
        Debug.Log("knocking back player ");
        MapTileManager.Instance.MoveMarker(unitId, moveTile);
        PlayerUnitObject puo = GetPlayerUnitObject(unitId).GetComponent<PlayerUnitObject>(); ;
        puo.InitializeKnockback(moveTile);
        PlayerUnit pu = GetPlayerUnit(unitId);

        if(PhotonNetwork.offlineMode || PhotonNetwork.isMasterClient) //Other doesn't do falldamage, only master
            DoFallDamage(pu, pu.TileZ, moveTile.height);

        SetUnitTile(board, unitId, moveTile);
    }

    [PunRPC]
    public void KnockbackPlayerRPC(int unitId, int tileX, int tileY)
    {
        //only arguments that matter are first 3 and last 1
        CombatMultiplayerMove cmm = new CombatMultiplayerMove(unitId, tileX, tileY, false, NameAll.NULL_INT, isKnockback:true);
        this.PostNotification(MultiplayerMove, cmm);
    }

    void DoFallDamage(PlayerUnit actor, int startHeight, int endHeight)
    {
        if (actor.AbilityMovementCode == NameAll.MOVEMENT_FLY)
        {
            return;
        }
        int fallDistance = startHeight - endHeight;
        if (fallDistance > actor.StatTotalJump)
        {
            int fallDamage = (int)Math.Ceiling((fallDistance - (double)actor.StatTotalJump) * (double)actor.StatTotalMaxLife / 10);
            AlterUnitStat(NameAll.ALTER_STAT_DAMAGE, fallDamage, NameAll.STAT_TYPE_HP, actor.TurnOrder);
        }
    }

    //called if a weapon is broken, charge attack is broken
    public void CheckForChargeRemove(PlayerUnit actor)
    {
        int actorId = actor.TurnOrder;
        if (NameAll.IsClassicClass(actor.ClassId) && StatusManager.Instance.IfStatusByUnitAndId(actorId, NameAll.STATUS_ID_CHARGING)
                && SpellManager.Instance.RemoveArcherCharge(actorId))
        {
            //spellslow removed in the spellmanager check, status removed here
            StatusManager.Instance.RemoveStatus(actorId, NameAll.STATUS_ID_CHARGING);
        }
    }

    
    //called in CombatMoveSequenceState in online game.
    //if master, tells other what movement to mirror
    //if other receives an RPC, raises a notification, notification then starts the movement action
    //master has to do the move in CombatMoveSequenceState AND other has to be in gameeloop state or the timing will be fucked up
    public void ConfirmMove( Board board, PlayerUnit actor, Tile targetTile, bool isClassicClass, int swapUnitId )
    {
        
        if( !PhotonNetwork.offlineMode )
        {
            if( PhotonNetwork.isMasterClient)
            {
                int actorId = actor.TurnOrder;
                int tileX = targetTile.pos.x;
                int tileY = targetTile.pos.y;
                Debug.Log("sending confirm move to other");
                photonView.RPC("ConfirmMoveRPC", PhotonTargets.Others, new object[] { actorId, tileX, tileY, isClassicClass, swapUnitId });
            }
            else
            {
                StartCoroutine(ConfirmMoveInner(board, actor, targetTile, isClassicClass, swapUnitId));
            }
            
        }

    }

    const string MultiplayerMove = "Multiplayer.Move";

    //Other is doing the actual move, sends the coordinates P2 in the notification so P2 can do the move on its side
    [PunRPC]
    public void ConfirmMoveRPC(int actorId, int tileX, int tileY, bool isClassicClass, int swapUnitId)
    {
        //Debug.Log("other has received the move RPC, sending a notification so other can start the move");
        //sends a notification to P2, P2 gets it then calls ConfirmMove
        CombatMultiplayerMove cmm = new CombatMultiplayerMove(actorId, tileX, tileY, isClassicClass, swapUnitId,false);
        this.PostNotification( MultiplayerMove, cmm);
    }

    IEnumerator ConfirmMoveInner(Board board, PlayerUnit actor, Tile targetTile, bool isClassicClass, int swapUnitId)
    {
        

        //Debug.Log("other is moving a unit in confirmMoveInner 0");
        Tile actorStartTile = board.GetTile(actor); //used in case of movement ability swap

        MapTileManager.Instance.MoveMarker(actor.TurnOrder, targetTile);
        PlayerUnitObject puo = GetPlayerUnitObjectComponent(actor.TurnOrder);
        puo.GetTilesInRange(board, actorStartTile, actor); //movement relies on knowing link to previous tile (t.prev); this sets the links)
        puo.SetAnimation("moving", false);

        if (actor.IsSpecialMoveRange())
        {
            //Debug.Log("other is moving a unit in confirmMoveInner 1");
            if (isClassicClass)
            {
                if (actor.AbilityMovementCode == NameAll.MOVEMENT_FLY)
                    yield return StartCoroutine(puo.TraverseFly(targetTile));
                else if (actor.AbilityMovementCode == NameAll.MOVEMENT_TELEPORT_1)
                    yield return StartCoroutine(puo.TraverseTeleport(targetTile)); //Debug.Log("Decide on movement details and remove this yield return null");
                else
                    yield return StartCoroutine(puo.Traverse(targetTile));
            }
            else
            {
                if (actor.AbilityMovementCode == NameAll.MOVEMENT_UNSTABLE_TP
                    || actor.AbilityMovementCode == NameAll.MOVEMENT_WINDS_OF_FATE)
                    yield return StartCoroutine(puo.TraverseTeleport(targetTile));
                else if (actor.AbilityMovementCode == NameAll.MOVEMENT_LEAP)
                    yield return StartCoroutine(puo.TraverseFly(targetTile));
                else
                    yield return StartCoroutine(puo.Traverse(targetTile));
            }
        }
        else
        {
            //Debug.Log("other is moving a unit in confirmMoveInner 2");
            yield return StartCoroutine(puo.Traverse(targetTile));
        }
            

        puo.SetAnimation("idle", true);
        SetUnitTile(board, actor.TurnOrder, targetTile);

        yield return new WaitForFixedUpdate(); //not sure if needed but want at least a slight delay for the swap
        if (swapUnitId != NameAll.NULL_UNIT_ID)
            SetUnitTileSwap(board, swapUnitId, actorStartTile);

    }

    //called in CombatmoveSequenceState, sets the correct unit tile
    public void SetUnitTile(Board board, int unitId, Tile t)
    {
        //GetPlayerUnitObject(unitId); //set as the animation occurs
        board.UpdatePlayerUnitTile(GetPlayerUnit(unitId), t); //takes the old tile, sets it to null unit id, sets the new tile to current player Id
        GetPlayerUnit(unitId).SetUnitTile(t); //updates the PlayerUnit with the correct x,y,z
    }

    public void SetUnitTileSwap(Board board, int unitId, Tile t)
    {

        board.UpdatePlayerUnitTileSwap(GetPlayerUnit(unitId), t); //takes the old tile, sets it to null unit id, sets the new tile to current player Id
        GetPlayerUnit(unitId).SetUnitTile(t); //updates the PlayerUnit with the correct x,y,z
        MapTileManager.Instance.MoveMarker(unitId, t);
        StartCoroutine(GetPlayerUnitObjectComponent(unitId).TraverseTeleport(t)); //moves the swapped player
        
    }
    
    //Called in CombatEndFacingState if alive, ActiveTurnState if dead
    public void EndCombatTurn(CombatTurn turn, bool isDead = false)
    {
        if( !PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("EndCombatTurnRPC", PhotonTargets.Others, new object[] { turn.actor.TurnOrder, turn.endDir.DirectionToInt(), turn.hasUnitActed,
                turn.hasUnitMoved, isDead });
        }

        PlayerUnit actor = turn.actor;
        if (isDead)
        {
            turn.hasUnitActed = true; //turning this to true so full turn is decremented
            turn.hasUnitMoved = true;
            actor.EndTurnCT(turn);
        }
        else
        {
            actor.Dir = turn.endDir;
            SetPUODirectionEndTurn(actor.TurnOrder);
            //Debug.Log("do the playerunitobject turn shit here or in combatEndFacingState");
            actor.EndTurnCT(turn);
        }
    }

    [PunRPC]
    public void EndCombatTurnRPC(int unitId, int directionInt, bool hasUnitActed, bool hasUnitMoved, bool isDead)
    {
        PlayerUnit actor = GetPlayerUnit(unitId);
        if (isDead)
        {
            actor.EndTurnCT(hasUnitActed, hasUnitMoved);
        }
        else
        {
            actor.Dir = DirectionsExtensions.IntToDirection(directionInt);
            SetPUODirectionEndTurn(actor.TurnOrder);
            actor.EndTurnCT(hasUnitActed, hasUnitMoved);
        }
    }

    #region Calls to PlayerUnitObjects to cause turning

    //dir is in the actor. turn to the direction actor should be facing
    //NO RPC NEEDED, called only in EndCombatTurn and EndCombatTurnRPC
    public void SetPUODirectionEndTurn(int unitId)
    {
        GetPlayerUnitObjectComponent(unitId).SetFacingDirectionEndTurn(GetPlayerUnit(unitId));
    }

    //called in InitCombatState to set the initial facing direction of the unit
    //no RPC needed as both sides call it locally (forcing an RPC call would result in errors
    public void SetInitialFacingDirection(int unitId, Directions dir = Directions.North, bool isDefault = true)
    {
        if (isDefault)
        {
            if (GetPlayerUnit(unitId).TeamId == NameAll.TEAM_ID_GREEN)
            {
                GetPlayerUnit(unitId).Dir = Directions.East;

            }
            else
            {
                GetPlayerUnit(unitId).Dir = Directions.West;
            }
        }
        else
        {
            GetPlayerUnit(unitId).Dir = dir;
        }
        GetPlayerUnitObjectComponent(unitId).SetAttackDirection(GetPlayerUnit(unitId).Dir);
    }

    public void SetFacingDirectionMidTurn(int unitId, Tile startTile, Tile endTile) //based on actor and target's tiles, like jumping
    {
        Directions dir = startTile.GetDirectionAttack(endTile);
        GetPlayerUnit(unitId).Dir = dir;
        if (endTile.pos.x != startTile.pos.x || endTile.pos.y != startTile.pos.y)
        {
            SetPUODirectionMidTurn(unitId, dir);
        }
    }

    //called in combatperformabilitystate and calcMono
    public void SetFacingDirectionAttack(Board board, PlayerUnit actor, Tile targetTile)
    {
        Tile actorTile = board.GetTile(actor);
        Directions dir = actorTile.GetDirectionAttack(targetTile);
        if (targetTile.pos.x != actor.TileX || targetTile.pos.y != actor.TileY)
        {
            SetPUODirectionMidTurn(actor.TurnOrder, dir);
        }
    }

    //for turning mid-turn. forces a PUO to turn but does nothing permanent to the PU (so unit can turn back to its correct direction later)
    public void SetPUODirectionMidTurn(int unitId, Directions dir, bool isSend = true)
    {
        GetPlayerUnitObjectComponent(unitId).SetAttackDirection(dir);
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient && isSend) //some cases don't want to send since it's a mid turn action
            photonView.RPC("SetPUODirectionMidTurnRPC", PhotonTargets.Others, new object[] { unitId, dir.DirectionToInt() });
    }

    //other receives this from master from a slow action
    [PunRPC]
    public void SetPUODirectionMidTurnRPC(int unitId, int dirInt)
    {
        GetPlayerUnitObjectComponent(unitId).SetAttackDirection(DirectionsExtensions.IntToDirection(dirInt));
    }

    //online: master calls this on direction attacks so that other can see the turn
    //only called form combatconfirmabilitytargetstate when master is doing the input
    public void SetMPPUODirectionMidTurn(int unitId, Directions dir)
    {
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient )
            photonView.RPC("SetPUODirectionMidTurnRPC", PhotonTargets.Others, new object[] { unitId, dir.DirectionToInt() });
    }
    #endregion

    //Movement Ability Scale has a special jump value. called in PUO to get special jump value for scale (which only works for certain tiles)
    //could speed this up by doing a call directly to the playerunit
    public int GetJumpScale(int unitId, Tile t)
    {
        return GetPlayerUnit(unitId).GetJumpScale(t);
    }


    public List<PlayerUnit> GetAllUnitsByTeamId(int teamId, bool same_team = true)
    {
        List<PlayerUnit> temp = new List<PlayerUnit>();
        if (same_team)
        {
            //teamId = teamId;
        }
        else {
            if (teamId == 2)
            {
                teamId = 3;
            }
            else {
                teamId = 2;
            }
        }

        foreach (PlayerUnit p in sPlayerUnitList)
        {
            if (p.TeamId == teamId && !StatusManager.Instance.IfStatusByUnitAndId(p.TurnOrder, NameAll.STATUS_ID_CRYSTAL))
            {
                temp.Add(p);
            }
        }
        return temp;
    }

    //set able to fight: battle ends when all units on a side unable to fight (or other Victory Conditions)
    //online mode: master send RPC to other to toggle status
    [PunRPC]
    public void SetAbleToFight(int unitId, bool able)
    {
        GetPlayerUnit(unitId).SetAbleToFight(able);
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("SetAbleToFight", PhotonTargets.Others, new object[] { unitId, able });
        }
    }

    [PunRPC]
    public void AddLife(int effect, int unitId)
    {
        GetPlayerUnit(unitId).ReturnToLife(effect); //Debug.Log("adding life to a unit" + effect);
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("AddLife", PhotonTargets.Others, new object[] { effect, unitId });
        }
    }

    [PunRPC]
    public void RemoveMPById(int unitId, int mp)
    {
        GetPlayerUnit(unitId).RemoveMP(mp);
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("RemoveMPById", PhotonTargets.Others, new object[] { unitId, mp});
        }
    }

    //called in calculationResolveaction
    [PunRPC]
    public void AlterUnitStat(int alter_stat, int effect, int statType, int unitId, int element_type = 0)
    {
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("AlterUnitStat", PhotonTargets.Others, new object[] { alter_stat, effect, statType, unitId, element_type });
        }
        //Debug.Log("alter stat is " + alter_stat + " statType, elementType " + statType + ", " + element_type );
        SetPlayerObjectAnimation(unitId, "damage", false);
        if (statType == NameAll.STAT_TYPE_HP)
        {
            GetPlayerUnit(unitId).SetHP(effect, alter_stat, element_type, false);
        }
        else
        {
            //Debug.Log("altering unit stat " + effect);
            GetPlayerUnit(unitId).AlterStat(alter_stat, effect, statType, element_type);
        }
        
        //this.PostNotification(ActorStatChangeNotification, GetPlayerUnit(unitId)); //causes errors in multiplayer
    }

    const string ActorStatChangeNotification = "CombatUITarget.ActorStatChangeNotification"; //updates the actor panel

    [PunRPC]
    public void RemoveLife(int effect, int remove_stat, int unitId, int elemental_type, bool removeAll = false)
    {
        GetPlayerUnit(unitId).SetHP(effect, remove_stat, elemental_type, removeAll);
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("RemoveLife", PhotonTargets.Others, new object[] { effect, remove_stat, unitId, elemental_type, removeAll });
        }
    }

    public bool IsMimeOnTeam(int team_id)
    {
        //Debug.Log(" is mime on team " + isMimeOnGreen + isMimeOnRed);
        if (team_id == NameAll.TEAM_ID_GREEN)
        {
            return isMimeOnGreen;
        }
        else if (team_id == NameAll.TEAM_ID_RED)
        {
            return isMimeOnRed;
        }
        return false;
    }

    //called in InitCombatState, allows faster checking for if mime on team
    public void SetMimeOnTeam()
    {
        isMimeOnGreen = false;
        isMimeOnRed = false;
        foreach (PlayerUnit p in sPlayerUnitList)
        {
            if (p.ClassId == NameAll.CLASS_MIME)
            {
                if (p.TeamId == NameAll.TEAM_ID_GREEN)
                {
                    isMimeOnGreen = true;
                }
                else if (p.TeamId == NameAll.TEAM_ID_RED)
                {
                    isMimeOnRed = true;
                }
            }
        }
    }

    //called in InitCombatState, adds the lasting statuses (item based statuses and blocks)
    public void SetLastingStatuses()
    {
        foreach( PlayerUnit pu in sPlayerUnitList)
        {
            pu.SetLastingStatuses();
        }
    }

    //called in calculation resolve action, gets list of mimes
    public List<PlayerUnit> GetMimeList(int actorCharmTeam)
    {
        List<PlayerUnit> temp = new List<PlayerUnit>();
        foreach( PlayerUnit p in sPlayerUnitList)
        {
            if( p.ClassId == NameAll.CLASS_MIME && StatusManager.Instance.IsAbleToReact(p.TurnOrder ) && p.GetCharmTeam() == actorCharmTeam )
            {
                temp.Add(p);
            }
        }
        return temp;
    }

    public bool QuickFlagCheckPhase()
    {
        //tells the game loop, there's someone (anyone) with quick, thus it is their turn to act
        //might want a status check here at some point (or count on status's properly removing quick flags and not adding them to ineligible units
        foreach (PlayerUnit p in sPlayerUnitList)
        {
            if (p.IsQuickFlag())
            {
                Debug.Log("Found a unit with quick");
                return true;
            }
        }
        return false;
    }

    public int GetQuickFlagUnitId()
    {
        int z1 = NameAll.NULL_INT; //Debug.Log("getting a quick flag unit");
        foreach ( PlayerUnit p in sPlayerUnitList)
        {
            if( p.IsQuickFlag() && p.TurnOrder < z1 )
            {
                z1 = p.TurnOrder;
            }
        }
        if( z1 != NameAll.NULL_INT)
        {
            //Debug.Log("ending quick flag units turn");
            //this unit is getting a turn now, disabling the quick flag
            SetQuickFlag(z1, false);
            StatusManager.Instance.RemoveStatus(z1, NameAll.STATUS_ID_QUICK, true); //RemoveFromStatusList(z1, "quick"); handled in there

        }
        return z1;
    }

    //RPC not needed. When Quick added in statusmanager quick on both sides is called
    public void SetQuickFlag(int unitId, bool isQuick )
    {
        GetPlayerUnit(unitId).SetQuickFlag(isQuick);
        if (isQuick)
        {
            GetPlayerUnit(unitId).SetCT(100, "quick");
        }
        else
        {
            StatusManager.Instance.RemoveStatus(unitId, NameAll.STATUS_ID_QUICK);
        }
    }

    public bool IsAbilityEquipped(int unitId, int abilityId, int abilitySlot)
    {
        if(GetPlayerUnit(unitId).IsAbilityEquipped(abilityId,abilitySlot))
        {
            return true;
        }
        return false;
    }

    public int GetWeaponPower( int unitId, bool battleSkill = false, bool twoHandsTypeAllowed = false)
    {
        return GetPlayerUnit(unitId).GetWeaponPower(battleSkill, twoHandsTypeAllowed);
    }

    //not using this, using on the size of the SpellManager SpellReaction Queue
    //public bool IsReactionFlagActive() 
    //{
    //    foreach (PlayerUnit p in sPlayerUnitList)
    //    {
    //        if (p.IsReactionFlag() )
    //        {
    //            return true;
    //        }
    //    }
    //    return false;
    //}

    //set to inactive in SpellManager, set to active in one of the Calculations
    public void AlterReactionFlag(int unitId, bool reactionFlag)
    {
        if (reactionFlag)
        {
            GetPlayerUnit(unitId).EnableReactionFlag();
        }
        else
        {
            GetPlayerUnit(unitId).DisableReactionFlag();
        }
    }

    public void EquipMovementAbility(int unitId, int abilityId) //calls unequip first in playerunit
    {
        GetPlayerUnit(unitId).EquipMovementAbility(abilityId);
    }

    public Tile GetPlayerUnitTile(Board board, int unitId)
    {
        var pu = GetPlayerUnit(unitId);
        Point p = new Point(pu.TileX,pu.TileY);
        return board.GetTile(p);
    }
    
    //called in initcombatsatte
    public void InitializePlayerUnits()
    {
        foreach(PlayerUnit pu in sPlayerUnitList)
        {
            pu.InitializeTwoSwordsEligible();
            pu.InitializeOnMoveEffect();
            pu.InitializeSpecialMoveRange();
            if( pu.ClassId >= NameAll.CUSTOM_CAMPAIGN_ID_START_VALUE)
            {
                ClassEditObject ce = CalcCode.LoadCustomClass(pu.ClassId);
                if(ce != null)
                    SpellManager.Instance.GenerateSpellNamesByCustomCommandSet(ce.CommandSet);
            }
            else if(pu.AbilitySecondaryCode >= NameAll.CUSTOM_COMMAND_SET_ID_START_VALUE)
            {
                SpellManager.Instance.GenerateSpellNamesByCustomCommandSet(pu.AbilitySecondaryCode);
            }
        }
    }

    public bool IsEligibleForTwoSwords( int unitId, int commandSet)
    {
        if( commandSet == NameAll.COMMAND_SET_ATTACK_AURELIAN || commandSet == NameAll.COMMAND_SET_ATTACK || commandSet == NameAll.COMMAND_SET_BATTLE_SKILL)
        {
            //Debug.Log("in is pm is eligible for two swords");
            return GetPlayerUnit(unitId).IsEligibleForTwoSwords();
        }
        return false;   
    }

    //show text over a playerUnit
    [PunRPC]
    public void ShowFloatingText(int unitId, int type, string value, bool isCrit = false)
    {
        //Debug.Log("showing floating text " + type + " " + value );
        if( !PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            //showfloatingtext for 0, 7, 19 need notifications so other shows them
            if (type == 0 || type == 19 || type == 7)
                photonView.RPC("ShowFloatingText", PhotonTargets.Others, new object[] { unitId, type, value, isCrit });
        }

        if ( type == 0) //miss
        {
            OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.Miss, "MISS!");
        }
        else if( type == 1) //damage, not doing crit for now
        {
            OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.Hit, value);
            //if(isCrit)
            //{
            //    OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.CriticalHit, value);
            //}
            //else
            //{
            //    OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.Hit, value);
            //}
        }
        else if( type == 2) //heal
        {
            OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.Heal, value);
        }
        else if (type == 7) //status
        {
            OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.AddStatus, value);
        }
        else if (type == 5) //heal mp or other stat
        {
            //Debug.Log("showing heal mp?");
            OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.HealMP, value);
        }
        else if (type == 6) //damage mp or other stat
        {
            OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.HitMP, value);
        }
        else if(type == 19) //custome message
        {
            OverlayCanvasController.instance.ShowCombatText(GetPlayerUnitObject(unitId), CombatTextType.Miss, value);
        }

        //OverlayCanvasController.instance.ShowCombatText(hitObject, CombatTextType.Hit, value);
        //Debug.Log("asdf2");
    }

    //should update playerunit teamId with enums at some point
    public Teams CheckForDefeat()
    {
        Teams victor = Teams.None;
        bool team1Dead = true;
        bool team2Dead = true;

        foreach (PlayerUnit p in sPlayerUnitList)
        {
            if (team1Dead || team2Dead)
            {
                if (p.TeamId == NameAll.TEAM_ID_GREEN)
                {
                    if (p.AbleToFight)
                    {
                        team1Dead = false;
                    }
                }
                else
                {
                    if (p.AbleToFight)
                    {
                        team2Dead = false;
                    }
                }

            }
            else
            {
                break;
            }
        }

        if (team1Dead)
        {
            victor = Teams.Team2;
        }
        if (team2Dead)
        {
            victor = Teams.Team1;
        }

        return victor;
    }

    //called in game loop, returns 2 for team 2 win return 3 for team 3 win
        //should be obsolete at some point
    public int CheckEndGameConditions()
    {
        int z1 = 0;
        bool team2Dead = true;
        bool team3Dead = true;
        foreach( PlayerUnit p in sPlayerUnitList)
        {
            if(team2Dead || team3Dead)
            {
                if( p.TeamId == 2 )
                {
                    if( p.AbleToFight)
                    {
                        team2Dead = false;
                    }
                }
                else
                {
                    if (p.AbleToFight)
                    {
                        team3Dead = false;
                    }
                }
                
            }
            else
            {
                break;
            }
        }
        if( team2Dead)
        {
            return 3; //team 3 won
        }
        if(team3Dead)
        {
            return 2; //team 2 won
        }
        return z1;
    }

    //non lasting statuses created in status amanger
    //LASTING STATUSES FOR PLAYERUNITOBJECTS CREATED IN STATUS OBJECT CREATOR
    //[PunRPC] //calling PunRPC in statusManager to call this
    //Online: generic call done here, locally on each side. For specific call use SendMPStatusList
    public void AddToStatusList(int unitId, int statusId)
    {
        //Debug.Log("adding status to status list in player manager " + statusId + " " + unitId);
        //if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        //{
        //    photonView.RPC("AddToStatusList", PhotonTargets.Others, new object[] { unitId, statusId });
        //}
        PlayerUnitObject puo = GetPlayerUnitObject(unitId).GetComponent<PlayerUnitObject>();
        puo.AddToStatusList(NameAll.GetStatusString(statusId));
    }

    //called in status manager where applicable
    public void RemoveFromStatusList(int unitId, int statusId)
    {
        //Debug.Log("removing status from status list in player manager " + statusId + " " + unitId);
        PlayerUnitObject puo = GetPlayerUnitObject(unitId).GetComponent<PlayerUnitObject>();
        puo.RemoveFromStatusList(NameAll.GetStatusString(statusId));
        //if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        //{
        //    photonView.RPC("RemoveFromStatusListRPC", PhotonTargets.Others, new object[] { unitId, statusId });
        //}
    }

    //for Other, this is going to duplicate some calls (as the PunRPC is called after it is called locally) but won't result in an error or an issue in puo
    //[PunRPC]
    //public void RemoveFromStatusListRPC(int unitId, int statusId)
    //{
    //    PlayerUnitObject puo = GetPlayerUnitObject(unitId).GetComponent<PlayerUnitObject>();
    //    puo.RemoveFromStatusList(NameAll.GetStatusString(statusId));
    //}

    //offline: not used
    //online: called in StatusManager when Master wants to send Other a message to manipulate its status list
    [PunRPC]
    public void SendMPStatusList(bool isAdd, int unitId, int statusId)
    {
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("SendMPStatusList", PhotonTargets.Others, new object[] { isAdd, unitId, statusId });
            return;
        }
        //other receing the rpc
        if (isAdd)
            AddToStatusList(unitId, statusId);
        else
            RemoveFromStatusList(unitId,statusId);
    }



    public void EndOfTurnTick(int unitId, int type) //0 for poison, 1 for regen
    {
        PlayerUnit pu = GetPlayerUnit(unitId);
        int z1 = pu.StatTotalMaxLife / 8;
        if( type == 0)
        {
            if( CalculationResolveAction.MPSwitchCheck(GetPlayerUnit(unitId)) )
            {
                AlterUnitStat(1, z1, NameAll.STAT_TYPE_MP, unitId);
            }
            else
            {
                //pu.SetHP(z1, 1); //1 for the type that removes a stat
                RemoveLife(z1, 1, unitId, NameAll.ITEM_ELEMENTAL_NONE);
            }
            
        }
        else if( type == 1)
        {
            //pu.SetHP(z1, 0, "undead");
            RemoveLife(z1, 0, unitId, NameAll.ITEM_ELEMENTAL_UNDEAD);
        }
    }

    //called in character builder scene
    public void ClearPlayerLists()
    {
        sPlayerUnitList.Clear();
        sPlayerObjectList.Clear();
    }

    //called in status manager after invite hits
    public void DefectFromTeam(int unitId)
    {
        GetPlayerUnit(unitId).DefectFromTeam();
    }


    //List<PlayerUnit> RandomizePUList()
    //{
    //    List<PlayerUnit> puList = sPlayerUnitList.ToList();
    //    //var shuffledcards = cards.OrderBy(a => rng.Next());
    //    System.Random rng = new System.Random();
    //    puList = puList.OrderBy(a => rng.Next()).ToList();
    //    return puList;
    //}

    //List<int> RandomizeIntList(List<int> tempList)
    //{
    //    List<int> intList = tempList.ToList();
    //    //var shuffledcards = cards.OrderBy(a => rng.Next());
    //    System.Random rng = new System.Random();
    //    intList = intList.OrderBy(a => rng.Next()).ToList();
    //    return intList;
    //}

    //void RandomizeList<T>( List<T> shuffleList)
    //{
    //    List<T> tempList = shuffleList.ToList();
    //    //var shuffledcards = cards.OrderBy(a => rng.Next());
    //    System.Random rng = new System.Random();
    //    tempList = tempList.OrderBy(a => rng.Next()).ToList();
    //}

    const string MultiplayerDisableUnit = "Multiplayer.DisableUnit";
    //called from CombatStateActiveTurn when unit is crystallized and in CombatMoveSequence state
    [PunRPC]
    public void DisableUnit(int unitId)
    {
        if (!PhotonNetwork.offlineMode )
        {
            if (PhotonNetwork.isMasterClient)
                photonView.RPC("DisableUnit", PhotonTargets.Others, new object[] { unitId });
            else //other needs to make a call to board to disable the unit
                this.PostNotification(MultiplayerDisableUnit, unitId);
        }

        //PlayerUnit keeps the same tilex,tiley but is no longer physically there
        //moves the unit off the map
        Vector3 vec = new Vector3(-5, -5, -5);
        GetPlayerUnitObject(unitId).transform.position = vec;
        GetPlayerUnitObject(unitId).SetActive(false);
        GetPlayerUnit(unitId).SetCT(0); //crystal keeps it from going again, should be no other way for it to get a turn
        MapTileManager.Instance.DisableMarker(unitId); //moves marker off the map
    }


    [PunRPC]
    public void ToggleJumping(int unitId, bool isJumping)
    {
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
            photonView.RPC("ToggleJumping", PhotonTargets.Others, new object[] { unitId, isJumping });

        Vector3 mtoPos = MapTileManager.Instance.GetMarkerByIndex(unitId).transform.position; //MapTileManager.Instance.GetMapTileObjectByIndex(GetPlayerUnit(unitId).GetMap_tile_index()).transform.position;
        Vector3 vec;
        if (isJumping)
        {
            vec = new Vector3(0, 40, 0);
        }
        else
        {
            vec = new Vector3(0, mtoPos.y + NameAll.TILE_CENTER_HEIGHT, 0);
        }

        vec += mtoPos;
        GetPlayerUnitObject(unitId).transform.position = vec;
    }


    public bool IsOnTeam(int actorId, int targetId)
    {
        if (GetPlayerUnit(actorId).TeamId == GetPlayerUnit(targetId).TeamId)
            return true;

        return false;
    }

    //called in CombatComputerPlayer
    //he uses alliances, I'm using team Id
    public bool IsMatch(int actorId, int targetId, Targets targets)
    {
        //Debug.Log("is testing for targetId " + targetId);
        if (targetId == NameAll.NULL_UNIT_ID) //targetId fed in is tile.UnitId
            return false;

        bool isMatch = false;
        //Debug.Log("testing for target " + targets.ToString());
        switch (targets)
        {
            case Targets.Self:
                isMatch = actorId == targetId;
                break;
            case Targets.Ally:
                isMatch = IsOnTeam(actorId, targetId);
                break;
            case Targets.Foe:
                isMatch = !IsOnTeam(actorId, targetId);
                break;
        }
        //can add status effects down the line
        return isMatch;
    }

    //called in CombatComputerPlayer. Get lists of pu for actor to try to target first (ie target dead units with revive, hurt enemies with dmg etc)
    public List<PlayerUnit> GetAIList(int actorCharmTeam, int aiType)
    {
        List<PlayerUnit> retValue = new List<PlayerUnit>();
        if( aiType == NameAll.AI_LIST_HURT_ENEMY)
        {
            foreach(PlayerUnit pu in sPlayerUnitList)
            {
                if( pu.TeamId != actorCharmTeam)
                {
                    int z1 = pu.StatTotalLife * 2; //Debug.Log("testing for hurt enemy, hp is " + z1);
                    if (z1 > 0 && z1 < pu.StatTotalMaxLife)
                    {
                        retValue.Add(pu); //Debug.Log("adding hurt enemy to list");
                    }
                }
            }
        }
        else if( aiType == NameAll.AI_LIST_DEAD_ALLY)
        {
            foreach (PlayerUnit pu in sPlayerUnitList)
            {
                if (pu.TeamId == actorCharmTeam)
                {
                    if(StatusManager.Instance.IfStatusByUnitAndId(pu.TurnOrder, NameAll.STATUS_ID_DEAD))
                    {
                        retValue.Add(pu);
                    }
                }
            }
        }
        else if( aiType == NameAll.AI_LIST_HURT_ALLY)
        {
            foreach (PlayerUnit pu in sPlayerUnitList)
            {
                if (pu.TeamId == actorCharmTeam)
                {
                    if (pu.StatTotalLife * 2 > 0 && pu.StatTotalLife * 2 < pu.StatTotalMaxLife )
                    {
                        retValue.Add(pu);
                    }
                }
            }
        }
        return retValue;
    }

    #region multiplayer and multiplayer notifications
    const string MultiplayerActiveTurnPreTurn = "Multiplayer.ActiveTurnPreTurn";//pre active turn start, show whose turn it is. Other raises it to show PreTurn stuff
    const string MultiplayerCommandTurn = "Multiplayer.CommandTurn";//other gets to input an action
    const string MultiplayerActiveTurnMidTurn = "Multiplayer.ActiveTurnMidTurn";//active turn mid-turn. Master raises it after other has told results of input to Master and vice versa

    //Master calls this
    public void SendMPActiveTurnPreTurn(int unitId)
    {
        photonView.RPC("ReceiveMPActiveTurnPreTurn", PhotonTargets.Others, new object[] { unitId });
    }

    //Other receives this
    [PunRPC]
    public void ReceiveMPActiveTurnPreTurn(int unitId)
    {
        this.PostNotification(MultiplayerActiveTurnPreTurn, unitId);
    }

    //Master calls this
    public void SendMPActiveTurnStartTurn(int unitId, bool hasUnitActed, bool hasUnitMoved)
    {
        photonView.RPC("ReceiveMPActiveTurnStartTurn", PhotonTargets.Others, new object[] { unitId, hasUnitActed, hasUnitMoved });
    }

    //Other receives this
    [PunRPC]
    public void ReceiveMPActiveTurnStartTurn(int unitId, bool hasUnitActed, bool hasUnitMoved)
    {
        CombatTurn tempTurn = new CombatTurn();
        tempTurn.hasUnitActed = hasUnitActed;
        tempTurn.hasUnitMoved = hasUnitMoved;
        this.PostNotification(MultiplayerCommandTurn, tempTurn);
    }

    //online: master calls this to other, let's other show master's inputs (not the results but what was selected)
    //online: other calls this to master. tells results of input
    public void SendMPActiveTurnInput(bool isMove, bool isAct, bool isWait, int actorId, int tileX, int tileY, int directionInt,
        int targetId, int spellIndex, int spellIndex2)
    {
        //Debug.Log("calling send MPActiveTurnInput: " + actorId + " x,y " + tileX + "," + tileY + " " + directionInt);
        photonView.RPC("ReceiveMPActiveTurnInput", PhotonTargets.Others, new object[] { isMove, isAct, isWait, actorId, tileX, tileY, directionInt, targetId, spellIndex, spellIndex2 });
    }

    [PunRPC]
    public void ReceiveMPActiveTurnInput(bool isMove, bool isAct, bool isWait, int actorId, int tileX, int tileY, int directionInt,
        int targetId, int spellIndex, int spellIndex2)
    {
        //Debug.Log("receiving MPActiveTurnInput: " + actorId + " x,y " + tileX + "," + tileY + " " + directionInt);
        CombatMultiplayerTurn cmt = new CombatMultiplayerTurn(isMove, isAct, isWait, actorId, tileX, tileY, directionInt, targetId, spellIndex, spellIndex2);
        this.PostNotification(MultiplayerActiveTurnMidTurn, cmt);
    }

    //Called in MPGameController to set game to online and who is MasterClient
    public void SetMPOnline(bool isMasterClient)
    {
        sMPObject.IsOffline = false;
        sMPObject.IsReady = false;
        sMPObject.IsOpponentReady = false;
        sMPObject.IsMasterClient = isMasterClient;
    }

    //called by Other in GameLoopState after a notification,
    //currently not needed but if moved from Master checking update constantly to a notification based system, this would be needed (in case Master queries Other)
    public void SetMPSelfStatusAndPhase(bool isReady, Phases phase)
    {
        sMPObject.IsReady = isReady;
        sMPObject.SelfCurrentPhase = phase;
    }

    //called by Master in GameLoopState and MultiplayerWaitPhase
    //master realizes other is not ready
    //other realizes that master is now waiting for it and does the necessary phase
    public void SendMPPhase(Phases phase)
    {
        sMPObject.IsOpponentReady = false;
        sMPObject.OpponentCurrentPhase = phase;
        if (PhotonNetwork.isMasterClient)
            photonView.RPC("ReceiveMPPhase", PhotonTargets.Others, (byte)phase);
    }

    const string MultiplayerGameLoop = "Multiplayer.GameLoop";

    [PunRPC]
    public void ReceiveMPPhase(byte phaseType)
    {
        sMPObject.SelfCurrentPhase = (Phases)phaseType;
        this.PostNotification(MultiplayerGameLoop, sMPObject.SelfCurrentPhase);
    }

    //Called by master in MultiplayerWaitState so that master knows that other is not ready and not in standby phase
    public void SetMPOpponentReadyAndPhase(bool isReady, Phases phase)
    {
        sMPObject.IsOpponentReady = isReady;
        sMPObject.OpponentCurrentPhase = phase;
    }

    //Called by Master in Online game. Checks to make sure game can move to next phase.
    public bool IsOpponentInStandbyAndReady()
    {
        if (sMPObject.IsOpponentReady && sMPObject.OpponentCurrentPhase == Phases.Standby)
            return true;

        return false;
    }

    //called by Other in GameLoopState in Online game. Lets Master know that Other is ready for the next phase
    //Other self update currently not needed but if moved from Master checking update constantly to a notification based system, this would be needed (in case Master queries Other)
    public void SetMPStandby()
    {
        //Debug.Log("letting master know that ready now");
        sMPObject.IsReady = true;
        sMPObject.SelfCurrentPhase = Phases.Standby;
        photonView.RPC("SendMPStandby", PhotonTargets.Others, new object[] { });
    }

    //received by Master in Online game, lets Master know that other is ready for the next phase
    [PunRPC]
    public void SendMPStandby()
    {
        sMPObject.IsOpponentReady = true;
        sMPObject.OpponentCurrentPhase = Phases.Standby;
        //can send notification here but already checking for it in GameLoopState
    }

    //called at beginning of GameLoopState to know if MP game or not
    public bool IsOfflineGame()
    {
        return sMPObject.IsOffline;
    }

    //called at beginning of GameLoopState to know if MC
    public bool isMPMasterClient()
    {
        return sMPObject.IsMasterClient;
    }

    //for debugging in multiplayer, goes on turns button
    public string GetMPSelfPhase()
    {
        return sMPObject.SelfCurrentPhase.ToString();
    }

    //online: called by master to tell P2 what to do with crystalized unit
    public void SendMPCrystalOutcome(List<int> tempList)
    {
        photonView.RPC("ReceiveMPCrystalOutcome", PhotonTargets.Others, new object[] { tempList[0], tempList[1], tempList[2] });
    }

    const string DidStatusManager = "StatusManager.Did";

    [PunRPC]
    public void ReceiveMPCrystalOutcome(int statusId, int unitId, int willCrystalize)
    {
        List<int> tempList = new List<int>();
        tempList.Add(statusId);
        tempList.Add(unitId);
        tempList.Add(willCrystalize);

        this.PostNotification(DidStatusManager, tempList);
    }

    //online: called by master to tell P2 to remove an item on the board
    public void SendMPRemoveTilePickUp(int tileX, int tileY, bool isEnable, int tilePickUpType)
    {
        //for now just removing but in future can use isEnable to add
        photonView.RPC("ReceiveMPRemoveTilePickUp", PhotonTargets.Others, new object[] { tileX, tileY, tilePickUpType });
    }

    const string MultiplayerTilePickUp = "Multiplayer.RemoveTilePickUp";

    [PunRPC]
    public void ReceiveMPRemoveTilePickUp(int tileX, int tileY, int tilePickUpType)
    {
        List<int> tempList = new List<int>();
        tempList.Add(tileX);
        tempList.Add(tileY);
        tempList.Add(tilePickUpType);

        this.PostNotification(MultiplayerTilePickUp, tempList);
    }

    [PunRPC]
    public void SendMPQuitGame(bool isSelfQuit)
    {
        if(!PhotonNetwork.offlineMode)
        {
            photonView.RPC("SendMPQuitGame", PhotonTargets.Others, new object[] { !isSelfQuit });
        }
        this.PostNotification(NameAll.NOTIFICATION_EXIT_GAME, isSelfQuit);
        
    }

    const string MultiplayerGameOver = "Multiplayer.GameOver";

    //online: called by Master in CombatCutSceneState to tell Other game is over
    [PunRPC]
    public void SendMPGameOver()
    {
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("SendMPGameOver", PhotonTargets.Others, new object[] { });
        }
        else
        {
            this.PostNotification(MultiplayerGameOver);
        }
    }

    const string MultiplayerMessageNotification = "Multiplayer.Message";

    [PunRPC]
    public void SendMPBattleMessage(string zString)
    {
        if (!PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("SendMPBattleMessage", PhotonTargets.Others, new object[] { zString });
        }
        else
        {
            this.PostNotification(MultiplayerMessageNotification, zString);
        }
    }

    //obviously better to use Photon.PunBehavour OnPhotonPlayerDisconnected but that requires the main class inheriting from Photon.PunBehaviour
    public int GetMPNumberOfPlayers()
    {
        sMPObject.NumberOfPlayers = PhotonNetwork.playerList.Length;
        return sMPObject.NumberOfPlayers;
    }

    //used by master at start of certain states to set all to false, only turned to true when both sides ready or master override (which shouldn't be needed)
    //[PunRPC]
    //public void SetMPReadyStatusAll(bool isReady, bool isOpponentReady)
    //{
    //    sMPObject.IsReady = isReady;
    //    sMPObject.IsOpponentReady = isOpponentReady;
    //    if( !PhotonNetwork.offlineMode && PhotonNetwork.isMasterClient)
    //        photonView.RPC("SetMPReadyStatusAll", PhotonTargets.Others, new object[] { isReady,isOpponentReady });

    //}

    //public void SetMPSelfReadyStatus(bool isReady)
    //{
    //    sMPObject.IsReady = isReady;
    //    if (!PhotonNetwork.offlineMode)
    //        photonView.RPC("SendMPReadyStatus", PhotonTargets.Others, new object[] { isReady});
    //}

    //[PunRPC]
    //public void SendMPReadyStatus(bool readyStatus)
    //{
    //    sMPObject.IsOpponentReady = readyStatus;
    //}
    #endregion

    public void SetPhotonNetworkOfflineMode(bool isOffline)
    {
        PhotonNetwork.offlineMode = true;
    }

}

