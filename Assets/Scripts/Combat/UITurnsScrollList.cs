﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class UITurnsScrollList : MonoBehaviour {

    public Button backButton;
    public GameObject sampleButton;
    public Transform contentPanel;
    List<TurnObject> spellNameList = new List<TurnObject>();
    UIBackButton backButtonUI;

    // Use this for initialization
    void Start()
    {
        backButtonUI = backButton.GetComponent<UIBackButton>();
    }

    public void Open()
    {
        gameObject.SetActive(true);
        //Debug.Log("turns menu opened");
        PopulateTurnsNames();
        
        //int commandSet;
        //if (SceneCreate.phaseMenu == 4) //secondary
        //{
        //    commandSet = PlayerManager.Instance.GetPlayerUnit(SceneCreate.active_unit).AbilitySecondaryCode;
        //}
        //else
        //{
        //    commandSet = PlayerManager.Instance.GetPlayerUnit(SceneCreate.active_unit).ClassId;
        //}
        //gameObject.SetActive(true);
        //spellNameList.Clear();
        ////Debug.Log("list size is " + spellNameList.Count);
        //PopulateSpellNames(commandSet);
    }

    public void Close()
    {
        //SceneCreate.menuMenu.Open();
        gameObject.SetActive(false);
    }

    public void ActBack()
    {
        //closes this menu and reopens the other menu
        Close();
        //MapTileManager.Instance.UnhighlightAllTiles();
    }

    void PopulateTurnsNames()
    {
        //Debug.Log("populating turns names neu");
        foreach (Transform child in contentPanel)
        {
            GameObject.Destroy(child.gameObject);
        }

        TurnsManager.Instance.RecreateTurnsList();
        foreach (TurnObject t in TurnsManager.Instance.GetTurnsList())
        {
            GameObject newButton = Instantiate(sampleButton) as GameObject;
            UITurnsListButton tb = newButton.GetComponent<UITurnsListButton>();

            int tempInt = t.GetTurnId();
            tb.title.text = " "  + tempInt + ": " + t.GetTitle();
            tb.transform.SetParent(contentPanel);
            tb.turnsIndex = tempInt;
            
            Button tempButton = tb.GetComponent<Button>();
            SetButtonColor(newButton, t.GetTeamId());
            tempButton.onClick.AddListener(() => ButtonClicked(tempInt));
        }
    }

    void SetButtonColor(GameObject go, int teamId)
    {
        if (teamId == NameAll.TEAM_ID_GREEN)
        {
            go.GetComponent<Image>().sprite = Resources.Load<Sprite>("menu_team_1");
        }
        else if (teamId == NameAll.TEAM_ID_RED)
        {
            go.GetComponent<Image>().sprite = Resources.Load<Sprite>("menu_team_2");
        }
        else
        {
            go.GetComponent<Image>().sprite = Resources.Load<Sprite>("menu_neutral");
        }
    }

    //void PopulateSpellNames(int commandSet)
    //{
    //    foreach (Transform child in contentPanel)
    //    {
    //        GameObject.Destroy(child.gameObject);
    //    }
    //    spellNameList = SpellManager.Instance.GetSpellNamesByCommandSet(commandSet);
    //    foreach (SpellName sn in spellNameList)
    //    {
    //        GameObject newButton = Instantiate(sampleButton) as GameObject;
    //        UISampleButton sb = newButton.GetComponent<UISampleButton>();
    //        sb.spellName.text = sn.GetSpellName();
    //        sb.transform.SetParent(contentPanel);
    //        sb.spellId = sn.GetSpellId();
    //        //sb.button.onClick = myClick;
    //        int tempInt = sn.GetIndex();

    //        //sb.button.onClick.AddListener(() => ButtonClicked(tempInt));
    //        Button tempButton = sb.GetComponent<Button>();
    //        //int tempInt = i;

    //        tempButton.onClick.AddListener(() => ButtonClicked(tempInt));
    //    }
    //}

    void ButtonClicked(int turnsIndex)
    {
        //ADD CODE HERE TO SHOW WHERE ON THE MAP THE CLICK IS
        Debug.Log("Yatta, turns button pressed..."+turnsIndex);
        if( turnsIndex == NameAll.NULL_UNIT_ID) //hypothetical turn, can't click on it
        {
            return;
        }
    }

    //called from UIMenuMenu after receiving a notification from an ability list click
    public void AddSpellName(PlayerUnit pu, SpellName sn)
    {
        foreach (Transform child in contentPanel)
        {
            GameObject.Destroy(child.gameObject);
        }

        TurnsManager.Instance.InsertTurn(pu, sn);
        //don't need to recreate, already created
        //TurnsManager.Instance.RecreateTurnsList();
        foreach (TurnObject t in TurnsManager.Instance.GetTurnsList())
        {
            GameObject newButton = Instantiate(sampleButton) as GameObject;
            UITurnsListButton tb = newButton.GetComponent<UITurnsListButton>();

            int tempInt = t.GetTurnId();
            if( t.GetTurnId() == NameAll.NULL_UNIT_ID) //hypothetical ability
                tb.title.text = " " + t.GetTitle();
            else
                tb.title.text = " " + tempInt + ": " + t.GetTitle();

            tb.transform.SetParent(contentPanel);
            tb.turnsIndex = tempInt;

            Button tempButton = tb.GetComponent<Button>();
            SetButtonColor(newButton, t.GetTeamId());
            tempButton.onClick.AddListener(() => ButtonClicked(tempInt));
        }
    }
}
