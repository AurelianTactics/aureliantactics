﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIMainMenu : MonoBehaviour {

    public Button multiPlayerButton;
    public Button singlePlayerButton;
    public Button customGameButton;
    public Button editUnitButton;
    public Button levelBuilderButton;


    //   // Use this for initialization
    //   void Start () {

    //}

    public void OnMPButtonClick() //goes to mp menu, then loads a custom game with the "mp" designation in player preferences
    {
        PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_TYPE, NameAll.CUSTOM_GAME_ONLINE);
        //PlayerPrefs.SetInt(NameAll.PP_COMBAT_ENTRY, NameAll.SCENE_CUSTOM_GAME); //redundant, done in MPGameController
        SceneManager.LoadScene(NameAll.SCENE_MP_MENU);
    }

    public void OnEditUnitButtonClick()
    {
        PlayerPrefs.SetInt(NameAll.PP_EDIT_UNIT_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_EDIT_UNIT);
    }

    public void OnCustomGameButtonClick() //goes straight to custom game with custom game designation
    {
        PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_TYPE, NameAll.CUSTOM_GAME_OFFLINE);
        SceneManager.LoadScene(NameAll.SCENE_CUSTOM_GAME);
    }

    public void OnLevelBuilderClick()
    {
        PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_LEVEL_BUILDER);
    }

    public void OnAbilityBuilderClick()
    {
        //PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_ABILITY_BUILDER);
    }

    public void OnCampaignBuilderClick()
    {
        //PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_CAMPAIGN_BUILDER);
    }

    public void OnClassBuilderClick()
    {
        //PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_CLASS_BUILDER);
    }

    public void OnCampaignsClick()
    {
        //PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_CAMPAIGNS);
    }

    public void OnStoryModeClick()
    {
        PlayerPrefs.SetInt(NameAll.PP_STORY_MODE_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_STORY_MODE);
    }

    public void OnStoryBuilderClick()
    {
        //PlayerPrefs.SetInt(NameAll.PP_CUSTOM_GAME_ENTRY, NameAll.SCENE_MAIN_MENU);
        SceneManager.LoadScene(NameAll.SCENE_STORY_BUILDER);
    }

    public void OnQuitClick()
    {
        //QUIT WORKS ONLY IN BUILT VERSIONS OF THE GAME Debug.Log("Asdf ");
        Application.Quit();
    }
}
