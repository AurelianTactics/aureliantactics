﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//public class LevelData : ScriptableObject 
//{
//	public List<Vector3> tiles;
//}

[System.Serializable]
public class LevelData
{

    public List<SerializableVector3> tiles;
    public string levelName;
    public List<SerializableVector3> spList; //(point.x,point.y, teamId) fucking serialization

    public int GetTeamSpawnPointsCount(int teamId)
    {
        int retValue = 0;
        foreach( SerializableVector3 sv in spList)
        {
            if ((int)sv.z == teamId)
                retValue += 1;
        }
        return retValue;
    }

    public int GetMaxZ()
    {
        int retValue = 10;
        foreach( SerializableVector3 t in tiles)
        {
            if (t.z > retValue)
                retValue = (int)t.z;
        }
        return retValue;
    }

    public int GetMaxX()
    {
        int retValue = 3;
        foreach (SerializableVector3 t in tiles)
        {
            if (t.x > retValue)
                retValue = (int)t.x;
        }
        return retValue + 1;
    }

    public int GetMaxY()
    {
        int retValue = 3;
        foreach (SerializableVector3 t in tiles)
        {
            if (t.y > retValue)
                retValue = (int)t.y;
        }
        return retValue + 1;
    }

}
